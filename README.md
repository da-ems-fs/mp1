# Masterprojekt 1
Analyse Kölner ELS Daten aus PFEIL und IGNIS sowie Aachener Einsatzdaten. 

## Methodik
Details zur Methodik in [methodology.md](methodology.md)

## Entwicklung
Die Skripte, Module und Notebooks, die im Rahmen dieses Projekts entwickelt wurden, stehen hier frei zur Verfügung. 

Um diese weiterzuentwickeln oder für eigene Daten anzupassen, beachten Sie auch folgende Hinweise:

### Hinweise für Entwickler:innen

Grundsätzlich sollte nur Code commitet werden, nicht aber Daten.

Code und Daten sind im Falle von Jupyter Notebooks (.ipynb) nicht klar voneinander zu trennen. Daher müssen Notebooks vorher gestrippt werden.  


- Vertrauliche Daten nur in `/data/confidential` und `/data/interim` speichern.

- Vertraulicher Daten ggf. durch .gitignore und insbesondere für ipnybs mit nbstripout als pre-commit hook verhindern.

Installation von `nbstripout` als pre-commit hook: 
```sh
pip install nbstripout
nbstripout --install
```
verifizieren mit
```sh
nbstripout --is-installed
```
oder 
```sh
nbstripout --status
```