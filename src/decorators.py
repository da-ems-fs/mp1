import pandas as pd

def compare_length(func):
    def wrapper(*args, **kwargs):
        print(f"row count before: {len(args[0].index)}")
        df = func(*args, **kwargs)
        print(f"row count  after: {len(df)}")
        return df
    return wrapper
