from src.decorators import compare_length
import pandas as pd

class Importers:
    def from_csv(path_to_folder):
        return pd.read_csv(f"{path_to_folder}pfeil.csv", parse_dates=['DATUHR', 'DATUHR.1', 'DATUHRANLEG', 'UHRALARM', 'UHR3', 'UHR4', 'UHR7', 'UHR8', 'UHR1', 'UHR2'], infer_datetime_format=True, low_memory=False)
    def set_datetime_index(df):
        return df.set_index("DATUHR")


class Cleaners:
    @compare_length
    def replace_1980_with_NaT(df):
        return df.replace(pd.to_datetime("1980-01-01 00:00:00"), pd.NaT)
    @compare_length
    def remove_index_NaNs(df):
        return df[df.index.isnull()==False].copy(True)
    @compare_length
    def remove_if_outside_valid_range(df, valid_date_ranges, print_minmax=False):
        df_valid = pd.DataFrame()
        for df_name, daterange in valid_date_ranges.items():
            _df = df[(df["source"]==df_name) & (df.index >= daterange[0]) & (df.index < daterange[1])]
            df_valid = df_valid.append(_df)
            if print_minmax: 
                print(f"source name: {df_name}, valid from: {daterange[0]}, valid to: {daterange[1]}, actual min: {df[df['source']==df_name].index.min()}, actual max: {df[df['source']==df_name].index.max()}, clean min; {df_valid[df_valid['source']==df_name].index.min()}, clean max: {df_valid[df_valid['source']==df_name].index.max()}")
        return df_valid    
    @compare_length
    def add_unique_key_monthly(df):
        df["MonatsKey"] = df["DATUHR.1"].dt.year.round(0).astype(int).astype(str) + "-" + df["DATUHR.1"].dt.month.round(0).astype(int).astype(str) + "-" + df["EINSATZNR"].astype(int).astype(str)
        return df
    def extract_duration_in_seconds(df):
        df["duration"] = (df["UHR1"]-df["UHRALARM"]).dt.total_seconds()
        return df

class Filters:
    @compare_length
    def vehicle_type(df, vehicle_type):
        return df[df["MNAM1"].str.contains(vehicle_type)].copy()
    @compare_length
    def vehicle_types(df, vehicle_types):
        return df[df["MNAM1"].str.contains("|".join(vehicle_types))].copy()
    @compare_length
    def remove_incident_types(df, incident_types):
        return df[~df["STIWO"].isin(incident_types)].copy()
    @compare_length
    def remove_if_status_missing(df, status):
        return df[~df[status].isnull().all(1)].copy()
    @compare_length
    def remove_if_status_delta_larger(df, status_from, status_to, delta_seconds):
        df_temp = df[[status_from, status_to]]
        return df[df[status_from]+pd.Timedelta(seconds=delta_seconds) > df[status_to]]
    @compare_length
    def remove_if_status_missing_and_status_delta_larger(df, status_missing, status_from, status_to, delta_seconds):
        return df[~df[status_missing].isnull().all(1) | (df[status_missing].isnull().all(1) & (df[status_from]+pd.Timedelta(seconds=delta_seconds) > df[status_to]))].copy()

    
class Exporters:
    def full(df, path_to_folder):
        df.to_csv(f"{path_to_folder}pfeil.csv")
    def interop(df, path_to_folder):
        df_interop = df[["MonatsKey", "STIWO", "duration", "UHRALARM", "UHR1"]].copy()
        df_interop.index = df_interop.index.rename("datetime")
        df_interop = df_interop.rename(columns={"MonatsKey":"key", "STIWO":"code", "UHRALARM":"ts_dispatched", "UHR1":"ts_available"})
        df_interop.to_csv(f"{path_to_folder}pfeil_interop.csv")
