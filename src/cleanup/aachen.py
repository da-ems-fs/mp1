from src.decorators import compare_length
import pandas as pd

class Importers:
    datetime_columns = ["uhralarm", "uhr3", "uhr4", "uhr7", "uhr8", "uhr1", "uhr2"]
    def from_csv(path_to_folder):
        return pd.read_csv(f"{path_to_folder}aachen.csv", parse_dates=Importers.datetime_columns, infer_datetime_format=True, low_memory=False)

    def add_timezones(df):
        for col in Importers.datetime_columns:
            df[col] = df[col].dt.tz_localize("UTC").dt.tz_convert("Europe/Berlin")
        return df

    def add_datetime_for_indexing(df):
        df["datetime"] = df["uhralarm"]
        for col in Importers.datetime_columns:
            if col in df.columns:
                df["datetime"] = df["datetime"].fillna(df[col])
        #remove localization for indexing, converting to naive localtime
        df["datetime"] = df["datetime"].dt.tz_localize(None)
        return df

    def set_datetime_index(df):
        return df.set_index("datetime")

class Cleaners:
    @compare_length
    def remove_index_NaNs(df):
        return df[df.index.isnull()==False].copy(True)
    def extract_duration_in_seconds(df):
        df["duration"] = (df["uhr1"]-df["uhralarm"]).dt.total_seconds()
        return df

class Filters:
    @compare_length
    def vehicle_type(df, vehicle_type):
        return df[df["typ"].str.contains(vehicle_type)].copy()
    @compare_length
    def vehicle_types(df, vehicle_types):
        return df[df["typ"].str.contains("|".join(vehicle_types))].copy()
    @compare_length
    def remove_incident_types(df, incident_types):
        return df[~df["einsatzart"].isin(incident_types)].copy()
    @compare_length
    def remove_if_status_missing(df, status):
        return df[~df[status].isnull().all(1)].copy()
    @compare_length
    def remove_if_status_delta_larger(df, status_from, status_to, delta_seconds):
        df_temp = df[[status_from, status_to]]
        return df[df[status_from]+pd.Timedelta(seconds=delta_seconds) > df[status_to]]
    @compare_length
    def remove_if_status_missing_and_status_delta_larger(df, status_missing, status_from, status_to, delta_seconds):
        return df[~df[status_missing].isnull().all(1) | (df[status_missing].isnull().all(1) & (df[status_from]+pd.Timedelta(seconds=delta_seconds) > df[status_to]))].copy()

class Exporters:
    def full(df, path_to_folder):
        df.to_csv(f"{path_to_folder}aachen.csv")
    def interop(df, path_to_folder):
        df_interop = df[["einsatznummer", "einsatzstichwort", "duration", "uhralarm", "uhr1"]].copy()
        df_interop.index = df_interop.index.rename("datetime")
        df_interop = df_interop.rename(columns={"einsatznummer":"key", "einsatzstichwort":"code", "uhralarm":"ts_dispatched", "uhr1":"ts_available"})
        df_interop.to_csv(f"{path_to_folder}aachen_interop.csv")
        return df_interop