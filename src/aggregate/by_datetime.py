from src.decorators import compare_length
import pandas as pd
import numpy as np

def count_by(df, freq, get_by_code_category=False):
    return agg_by(df, freq, aggfunc=count, get_by_code_category=get_by_code_category)


def agg_by(df, freq, aggfunc, get_by_code_category=False):
    grouper = pd.Grouper(freq=freq)
    if not get_by_code_category:
        return aggfunc(df, grouper)
    else:
        df_aggd = aggfunc(df, grouper)
        df_aggd_cat = aggfunc(df, grouper, by_categories=True)
        return pd.merge(df_aggd, df_aggd_cat, left_index=True, right_index=True)

def count(df, grouper, by_categories=False):
    if not by_categories: 
        return df.groupby(grouper).count()["key"].copy().rename(index="incident_count")
    else:
        return df.groupby([grouper, 'code_category']).count()["key"].unstack()

def mean(df, grouper, by_categories=False):
    if by_categories: 
        raise NotImplementedError
    return df.groupby(grouper).mean()["incident_duration"].copy().rename(index="mean_incident_duration")

def _outlier_limit_iqr(df):
    dur = df["incident_duration"]
    q1 = dur.quantile(0.25)
    q3 = dur.quantile(0.75)
    iqr = q3 - q1
    lim = q3 + (iqr * 1.5)
    return lim

def _outlier_limit_quantile(df, val):
    dur = df["incident_duration"]
    lim = dur.quantile(val)
    return lim

class Importers:
    def from_csv(path_to_folder, filename):
        df = pd.read_csv(f"{path_to_folder}{filename}", parse_dates=["datetime", "ts_dispatched", "ts_available"], infer_datetime_format=True, index_col = ["datetime"], low_memory=False)
        for utc_column in ["ts_dispatched", "ts_available"]:
            df[utc_column] = pd.to_datetime(df[utc_column], utc=True)
        return df

class Aggregators:
    def get_incidents_from_alerts(df):
        df = df.copy()
        df["datetime"] = df.index
        groups = df.groupby("key")
        df_g = groups.agg({"datetime":"first", "code":"first", "code_category":"first", "ts_dispatched":"min","ts_available":"max"})
        df_g["key"] = df_g.index
        df_g = df_g.set_index("datetime")
        df_g = df_g.rename(columns={"ts_dispatched":"ts_start", "ts_available":"ts_end"})
        return df_g

    def get_duration_per_incident(df):
        df["incident_duration"] = (df["ts_end"]-df["ts_start"]).dt.total_seconds()
        return df

    def remove_durations_below_0s(df):
        df["incident_duration"].where(df["incident_duration"]>0, np.nan, inplace=True)
        return df

    def remove_duration_outliers_by(df, lim_comp="iqr"):
        if lim_comp == "iqr":
            lim = _outlier_limit_iqr(df)
        elif lim_comp == "q0.99":
            lim = _outlier_limit_quantile(df, 0.99)
        elif lim_comp == "hrs24":
            lim = 24*60*60
        elif lim_comp == "hrs12":
            lim = 12*60*60
        else:
            raise NotImplementedError(f"The outlier removal method {lim_comp} is not implemented!")
        print(f"limit is {round(lim/60, 2)} minutes; {sum(df['incident_duration']>lim)} values replaced with NaN")
        df["incident_duration"].where(df["incident_duration"]<lim, np.nan, inplace=True)
        return df

    def get_count_by_day(df, get_by_code_category=False):
        return count_by(df, "d", get_by_code_category)

    def get_count_by_hour(df, get_by_code_category=False):
        return count_by(df, "h", get_by_code_category)

    def get_mean_incident_duration_by_day(df, get_by_code_category=False):
        return agg_by(df, "d", mean)


class Exporters:
    def export_incidents(df, path_to_folder, name="NO-NAME"):
        df.to_csv(f"{path_to_folder}einsaetze_{name}.csv")

    def export_aggregated(series, path_to_folder, name="NO-NAME", aggregation_name="NO-AGG-NAME"):
        series.to_csv(f"{path_to_folder}einsaetze_{name}_pro_{aggregation_name}.csv")
