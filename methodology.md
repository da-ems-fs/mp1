Beschreibt `Version 0.9`. 
Dokument wird nicht zwangsläufig aktuell gehalten, daher auch Quellcode vergleichen. 

## Datenbeschaffung
Übergabe der Dateien in folgenden Formaten:
- Koeln: .xslx; Worksheets pro Jahr, 2020 getrennt nach ELS, bedingt durch Systemwechsel
- Aachen: 2x .csv, 2018-2019 und 2020

## Expoloration
__Explorative Phase.__

Zu Beginn und durchgehend während der Analyse. 
Explorative und experimentelle Versuche, häufig mit Plots. 

Hier entstehen Prototypen für Implementationen in späteren Arbeitsschritten.  
Vieles wird ausprobiert, das meiste verworfen, das bewährte dann in den folgenden Schritten "sauber" neu implementiert.  

## Import
__Einlesen der Originaldateien.__

Koeln: Zusammenfügen aller Daten nach ELS 
- -> pfeil.csv; ignis.csv

Aachen: Zusammenfügen aller Daten 
- -> aachen.csv

## Filter
__Vorbereiten und säubern der Daten.__

Hier sollten also nur noch grundsätzlich valide Alarmierungen von RTW, NEF und KTW übrig bleiben. 
Dabei werden insbesondere Info- und Testalarmierungen entfernt. 

### Koeln: 
- Allgemein: 
    - Stichworte, die entfernt werden: 
        - ["INFO","INFORET","TESTX1","TEST373","TESTF1","TESTVGONG","D1","AMT", "PROBE", "RD1TH", "RD2TH", "RD1FEU", "RD2FEU"]
    - Fahrzeuge, die beibehalten werden: 
        - ["RTW", "KTW", "NEF"]
- PFEIL:
    - Löschen nicht-valider Zeitwerte, wie beispielsweise "0", "",  oder "1.1.1980", dem DOS-Epoche Wert.
    - Hinzufügen eines eindeutigen Einsatz-Keys, da die Einsatznummer bei 100.000 auf 0 zurückrollt. 
- IGNIS:
    - Löschen nicht valider Alarmierungszeiten
    - Extraktion des Haupt-Stichworts als erster String vor dem Trennzeichen "."

### Aachen: 
- Konvertierung von UTC nach Lokalzeit
- Wenn keine Alarmierungszeit vorliegt, etwa wenn bei vorgeplanten Einsätzen das Fahrzeug vom Disponenten manuell in den Einsatz gezogen wird, wird die erste valide Statuszeit eingefügt
- Entfernen aller Alarmierungen ohne validen Zeitstempel
- Fahrzeuge, die beibehalten wurden: 
    - ["RTW", "NEF", "KTW", "ITW"]
- Einsatzarten, die entfernt wurden: 
    - ["K", "A", "T", "P"]
    - Es bleiben übrig: ["N", "H", "F", "Ü"]

### Gültige Datumsbereiche

#### Koeln
- Einzelne Export-Dateien überlappen sich teilweise
    - z.B. vereinzelt Daten aus 2017 im Datensatz von 2018
- Daher filtern jeder Datenquelle auf ihren jeweils gültigen Datumsbereich

#### Aachen
- Daten überlappen nicht
- lediglich Beschnitt auf Betrachtungszeitraum nötig

## Vereinheitlichung der Daten
In diesem Schritt werden die gereinigten Daten zusammengefügt und vereinheitlicht. 

### Stichwortkategorien: 
Um der Vielzahl an Einsatzstichworten Herr zu werden, werden diese in Einsatzkategorien eingeteilt. Dadurch können Koelner und Aachener Daten verglichen werden. 

- prim_1: Primärrettung ohne NEF
- prim_2: Primärrettung mit NEF
- sek_1: Sekundärtransport ohne NA
- sek_2: Sekundärtransport mit NA
- prim_fw: Primäreinsätze im Rahmen von Brandschutz- und Hilfeleistungseinsätzen der Feuerwehr

#### Koeln: 
- Zusammenfügen der Datensätze aus pfeil und ignis
- Entfernen aller Alarmierungen vor dem 1.1.2018
- Stichwort-Kategorien: 

| Kategorie        | Stichworte           |
| ------------- |-------------:|
| prim_1      | INTERN1, CHIRU1, NEUROL1, KTR, KIND1, GEBURT1, GYNO1, INTERN., CHIRU.|
| prim_2      | INTERN2, CHIRU2, NEUROL2, KIND2, GEBURT2, GYNO2|
| sek_1      | VERLEG1, IVERLEG1, BABY1, PSYCH1, BETREU1, SCHWERTR., SCHWERTR1, IVERL1, SCHWERTR|
| sek_2      | VERLEG2, IVERLEG2, BABY2, PSYCH2, PSYCH3, BETREU2, SCHWERTR2, ECMO, IVERL2|
| prim_fw      | Alles andere|

#### Aachen: 
- Entfernen aller Alarmierungen vor dem 1.1.2018
- Sortieren des Indexes nach Zeitstempel
- Stichwort-Kategorien: 

| Kategorie        | Stichworte           |
| ------------- |-------------:|
| prim_1      | 0, 1, 1+, VU1|
| prim_2      | 2, VU2, VU3, MANV1, MANV2, EUMED*|
| sek_1      | KH-V1|
| sek_2      | KH-V2, KH-V3, KH-V4, KH-V5, KH-V5, KH-V6, KH-V7|
| prim_fw      | Alles andere|

## Aggregation
Aggregierung der Alarmierungen nach Einsätzen; Zählen der Einsätze nach Tag, Stunde etc. 

Einsätze werden zusammengefasst nach Einsatz-Key, also entspricht im Regelfall ein Einsatz-Key einem Einsatz, unabhängig davon, wie viele Einsatzmittel alarmiert wurden. 

Die Zusammenfassung nach Tagen und Stunden erfolgt nach Lokalzeit, um einen erwart- und auswertbaren Tagesgang zu erhalten. 
Das bedeutet aber auch, dass die Tage der Umstellung von Sommer- auf Winterzeit und vice versa entsprechend mehr bzw. weniger Stunden und damit Einsätze haben. 