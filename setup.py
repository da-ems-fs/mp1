from setuptools import setup

setup(name='src',
      version='1.3',
      description='Filter and analyse incident level datasets',
      license='MIT',
      packages=["src"]
)